﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Test_Generator
{
    /*this is the base class for all the questions.
    As such each question regardless of type shares the need for a question number, chapter number, and the actual question
    their will be a doubley linked list holding all the questions for each type so this class also has a next question and previous question reference
    */
    class consolidatedQuestions
    {
        public string[,] consolidatedmcQuestions = new string[10,8];
        public string[,] consolidatedtfQuestions = new string[9,4];
        public string[,] consolidatedoeQuestions = new string[7,4];

        public consolidatedQuestions()
        {

        }

        public string[,] ConsolidatedMCQuestions
        { get; set; }

        public string[,] ConsolidatedTFQuestions
        { get; set; }

        public string[,] ConsolidatedOEQuestions
        { get; set; }

        public void addMCQuestions(string[,] NewMCQuestions)
        {
            string[,] temp = new string[10, 8];
            if (consolidatedmcQuestions[0, 1] != null)
            {
                for (int i = 0; i < temp.GetLongLength(0); i++)
                {
                    for (int j = 0; j < 8; j++)
                    {
                        temp[i, i] = consolidatedmcQuestions[i, j];
                    }
                }

                consolidatedmcQuestions = new string[consolidatedmcQuestions.GetLongLength(0) + NewMCQuestions.GetLongLength(0), 8];

                for (int i = 0; i < consolidatedmcQuestions.Length; i++)
                {
                    for (int j = 0; j < 8; j++)
                    {
                        consolidatedmcQuestions[i, j] = temp[i, j];
                    }
                }

                for (int i = temp.Length - 1; i < consolidatedmcQuestions.Length; i++)
                {
                    for (int j = 0; j < 8; j++)
                    {
                        consolidatedmcQuestions[i, j] = NewMCQuestions[i - temp.Length, j];
                    }
                }
            }
            else
            {
                for (int i = 0; i < 10 ; i++)
                {
                    for (int j = 0; j < 8; j++)
                    {
                        consolidatedmcQuestions[i, j] = NewMCQuestions[i, j];
                    }
                }
            }
        }

        public void addTFQuestions(string[,] NewTFQuestions)
        {
            string[,] temp = new string[10, 4];
            if (consolidatedtfQuestions[0, 1] != null)
            {
                for (int i = 0; i < temp.GetLongLength(0); i++)
                {
                    for (int j = 0; j < 4; j++)
                    {
                        temp[i, i] = consolidatedtfQuestions[i, j];
                    }
                }

                consolidatedtfQuestions = new string[consolidatedtfQuestions.GetLongLength(0) + NewTFQuestions.GetLongLength(0), 4];

                for (int i = 0; i < consolidatedtfQuestions.Length; i++)
                {
                    for (int j = 0; j < 4; j++)
                    {
                        consolidatedtfQuestions[i, j] = temp[i, j];
                    }
                }

                for (int i = temp.Length - 1; i < consolidatedtfQuestions.Length; i++)
                {
                    for (int j = 0; j < 4; j++)
                    {
                        consolidatedtfQuestions[i, j] = NewTFQuestions[i - temp.Length, j];
                    }
                }
            }
            else
            {
                for (int i = 0; i < consolidatedtfQuestions.GetLongLength(0); i++)
                {
                    for (int j = 0; j < 4; j++)
                    {
                        consolidatedtfQuestions[i, j] = NewTFQuestions[i, j];
                    }
                }
            }
        }

        public void addOEQuestions(string[,] NewOEQuestions)
        {
            string[,] temp = new string[10, 4];
            if (consolidatedoeQuestions[0, 1] != null)
            {
                for (int i = 0; i < temp.GetLongLength(0); i++)
                {
                    for (int j = 0; j < 4; j++)
                    {
                        temp[i, i] = consolidatedoeQuestions[i, j];
                    }
                }

                consolidatedmcQuestions = new string[consolidatedoeQuestions.GetLongLength(0) + NewOEQuestions.GetLongLength(0), 4];

                for (int i = 0; i < consolidatedoeQuestions.Length; i++)
                {
                    for (int j = 0; j < 4; j++)
                    {
                        consolidatedoeQuestions[i, j] = temp[i, j];
                    }
                }

                for (int i = temp.Length - 1; i < consolidatedoeQuestions.Length; i++)
                {
                    for (int j = 0; j < 4; j++)
                    {
                        consolidatedoeQuestions[i, j] = NewOEQuestions[i - temp.Length, j];
                    }
                }
            }
            else
            {
                for (int i = 0; i < consolidatedoeQuestions.GetLongLength(0); i++)
                {
                    for (int j = 0; j < 4; j++)
                    {
                        consolidatedoeQuestions[i, j] = NewOEQuestions[i, j];
                    }
                }
            }
        }


        /*
                private int questionNum;
                private int chapterNum;
                private string questionText;
                private Question nextQuestion;
                private Question prevQuestion;
        */
        /*
        public int QuestionNum
        {
            get { return questionNum; }
            set { questionNum = value; }
        }

        public int ChapterNum
        {
            get { return chapterNum; }
            set { chapterNum = value; }
        }
        public string QuestionText
        {
            get { return questionText; }
            set { questionText = value; }
        }

        public Question NextQuestion
        {
            get { return nextQuestion; }
            set { nextQuestion = value; }
        }

        public Question PrevQuestion
        {
            get { return prevQuestion; }
            set { prevQuestion = value; }
        }
        */
    }
}
