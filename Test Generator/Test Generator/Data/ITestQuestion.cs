﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_Generator.Data
{
    internal interface ITestQuestion
    {
        string QuestionText { get; set; }

        ICollection<string> PossibleAnswers { get; }

        string CorrectAnswer { get; set; }

        bool RequiresHandGrading { get; }
    }
}
