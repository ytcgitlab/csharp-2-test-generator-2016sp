﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Test_Generator.Data
{
    [DataContract]
    internal class TfTestQuestion : ITestQuestion
    {
        public TfTestQuestion()
        {
            PossibleAnswers = new List<string>();
        }

        [DataMember]
        public ICollection<string> PossibleAnswers { get; }

        [DataMember]
        public string QuestionText { get; set; }

        [DataMember]
        public string CorrectAnswer { get; set; }

        public bool RequiresHandGrading => false;
    }
}
