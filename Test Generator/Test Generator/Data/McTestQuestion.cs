﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Test_Generator.Data
{
    [DataContract]
    internal class McTestQuestion : ITestQuestion
    {
        public McTestQuestion()
        {
            PossibleAnswers = new List<string>();
        }

        [DataMember]
        public ICollection<string> PossibleAnswers { get; }

        [DataMember]
        public string QuestionText { get; set; }

        public string CorrectAnswer { get; set; }

        public bool RequiresHandGrading => false;
    }
}
