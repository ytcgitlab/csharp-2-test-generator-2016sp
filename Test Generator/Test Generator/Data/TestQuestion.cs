﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_Generator.Data
{
    internal abstract class TestQuestion
    {
        public abstract string QuestionText { get; set; }

        public abstract ICollection<string> PossibleAnswers { get; }

        public abstract bool RequiresHandGrading { get; }
    }
}
