﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Test_Generator.Data
{
    [DataContract]
    internal class FbTestQuestion : ITestQuestion
    {
        [DataMember]
        public int BlankLength { get; set; }

        public FbTestQuestion()
        {
            PossibleAnswers = new List<string>();
        }

        [DataMember]
        public ICollection<string> PossibleAnswers { get; }

        public string QuestionText {
            get
            {
                var sb = new StringBuilder();

                for (int i = 0; i < BlankLength; ++i)
                {
                    sb.Append("_");
                }

                return string.Format(_questionText, sb);
            }

            set
            {
                if (value.Contains("{0}"))
                    _questionText = value;
            }
        }

        [DataMember]
        public string CorrectAnswer { get; set; }

        public bool RequiresHandGrading => !(PossibleAnswers.Count > 0);

        [DataMember]
        private string _questionText;
    }
}
