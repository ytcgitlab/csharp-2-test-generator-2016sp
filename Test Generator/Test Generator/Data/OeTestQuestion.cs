﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Test_Generator.Data
{
    [DataContract]
    internal class OeTestQuestion : ITestQuestion
    {
        [DataMember]
        public int BlankLines { get; set; }
        public OeTestQuestion()
        {
            PossibleAnswers = new List<string>();
        }

        [DataMember]
        public ICollection<string> PossibleAnswers { get; }

        public string QuestionText
        {
            get
            {
                var sb = new StringBuilder();

                for (int i = 0; i < BlankLines; ++i)
                {
                    sb.AppendLine();
                }

                return _questionText + sb;
            }

            set { _questionText = value; }
        }

        [DataMember]
        public string CorrectAnswer { get; set; }

        public bool RequiresHandGrading => true;

        [DataMember]
        private string _questionText;
    }
}
